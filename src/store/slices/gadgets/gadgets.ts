import {AxiosResponse} from "axios";
import {createAsyncThunk, createSlice} from "@reduxjs/toolkit";
import api from "../../../api/instance";
import {DEVICE_TYPE} from "../../../features/GadgetsScreen/constants";
import {LOADER_STATE} from "../../../constants";
import {IGadgetsItems, IResponceData, IResponceItem} from "../../../types/store";

interface IGadgetsState {
    selectFetchingGadgets: LOADER_STATE;
    gadgets?: IGadgetsItems | null;
}

const initialState: IGadgetsState = {
    selectFetchingGadgets: LOADER_STATE.IDLE,
    gadgets: null
};

export const fetchGadgetsAction = createAsyncThunk(
    '/gadgets',
    async (_: void, { rejectWithValue }) => {
        try {
            const result: AxiosResponse<IResponceData> = await api.get('/products');
            console.info('How many tims')
            return result.data;
        } catch(e) {
            rejectWithValue(null)
        }
    }
);


export const gadgetsSlice = createSlice({
    name: 'gadgets',
    initialState,
    reducers: {
    },
    extraReducers: (builder) => {
        builder
            .addCase(fetchGadgetsAction.pending, (state) => {
                state.selectFetchingGadgets = LOADER_STATE.PENDING;
            })
            .addCase(fetchGadgetsAction.fulfilled, (state, action) => {
                state.selectFetchingGadgets = LOADER_STATE.SUCCESS;
                state.gadgets = action.payload?.products.reduce((acc: IGadgetsItems, item: IResponceItem) => {
                    if (Object.values(DEVICE_TYPE).includes(item.category)){
                        acc[item.category].data.push([item.title, item.rating])
                        acc[item.category].x.push(item.title)
                        acc[item.category].y.push(item.rating)
                    }
                    return acc
                }, {smartphones: {data: [], x: [], y: []}, laptops: {data: [], x: [], y: []}})
            })
            .addCase(fetchGadgetsAction.rejected, (state) => {
                state.selectFetchingGadgets = LOADER_STATE.FAILED;
            });
    },
});

// Selectors
type TSelectorState = { gadgets: IGadgetsState };

export const selectFetchingGadgets = (state: TSelectorState) =>
    state.gadgets.selectFetchingGadgets;

export const selectGadgets = (state: TSelectorState) => state.gadgets.gadgets;

export default gadgetsSlice.reducer;
