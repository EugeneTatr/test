import {useDispatch} from "react-redux";
import {useEffect, useState} from "react";
import {Grid, SelectChangeEvent} from "@mui/material";
import {AppDispatch} from "../../store";
import {DEVICE_TYPE} from "./constants";
import {fetchGadgetsAction} from "../../store/slices/gadgets/gadgets";
import DeviceSelect from "./components/DeviceSelect";
import ColumnGraph from "./components/ColumnGraph";


const GadgetScreen = () => {
    const dispatch: AppDispatch = useDispatch();
    const [current, setCurrent] = useState<DEVICE_TYPE>(DEVICE_TYPE.smartphones)

    useEffect(() => {
        dispatch(fetchGadgetsAction())
    }, [])

    const handleChangeSelect = (e: SelectChangeEvent<DEVICE_TYPE>) => {
        setCurrent(e.target.value as DEVICE_TYPE)
    }

    return (
        <Grid sx={{padding: 10}}>
            <DeviceSelect current={current} handleChangeSelect={handleChangeSelect}/>
            <ColumnGraph current={current} />
        </Grid>
    )
}

export default GadgetScreen