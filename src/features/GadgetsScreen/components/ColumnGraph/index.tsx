import {FC, useRef} from "react";
import HighchartsReact from "highcharts-react-official";
import Highcharts from "highcharts";
import {useSelector} from "react-redux";
import {selectFetchingGadgets, selectGadgets} from "../../../../store/slices/gadgets/gadgets";
import {DEVICE_TYPE} from "../../constants";
import Loader from "../Loader";

interface IColumnGraph {
    current: DEVICE_TYPE
}

const ColumnGraph: FC<IColumnGraph> = ({current}) => {
    const loaderState = useSelector(selectFetchingGadgets)
    const chartComponentRef = useRef<HighchartsReact.RefObject>(null);
    const gadgets = useSelector(selectGadgets)

    const options: Highcharts.Options = (
        {
            title: {
                text: 'Gadgets'
            },
            xAxis: {
                categories: gadgets?.[current]?.x
            },
            yAxis: {
                title: {
                    text: 'Rating'
                },
                categories: gadgets?.[current]?.y
            },
            series: [
                {
                    type: 'column',
                    name: 'Devices',
                    data: gadgets?.[current]?.data
                }
            ]
        }
    )

    return (
        <Loader loaderState={loaderState}>
            <HighchartsReact
                highcharts={Highcharts}
                options={options}
                ref={chartComponentRef}
            />
        </Loader>
    )
}

export default ColumnGraph