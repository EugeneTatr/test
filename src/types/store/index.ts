import {DEVICE_TYPE} from "../../features/GadgetsScreen/constants";

export interface IResponceData {
    limit: number,
    products: IResponceItem[]
    skip: number,
    total: number
}

export interface IResponceItem {
    brand: string,
    category: DEVICE_TYPE,
    description: string
    discountPercentage: string,
    id: string,
    images: string[],
    price: number
    rating: string
    stock: number,
    thumbnail: string
    title: string
}

interface IGadgetItem {
    data: [string, string][],
    x: string[],
    y: string[]
}

export interface IGadgetsItems {
    smartphones: IGadgetItem,
    laptops: IGadgetItem
}