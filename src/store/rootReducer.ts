import {combineReducers} from "@reduxjs/toolkit";
import configurationReducer from './slices/gadgets/gadgets';

const rootReducer = combineReducers({
    gadgets: configurationReducer
});

export default rootReducer;