import {FC} from "react";
import {MenuItem, Select, SelectChangeEvent} from "@mui/material";
import {DEVICE_TYPE} from "../../constants";

interface IDeviceSelect {
    current: DEVICE_TYPE,
    handleChangeSelect: (e: SelectChangeEvent<DEVICE_TYPE>) => void
}

const DeviceSelect: FC<IDeviceSelect> = ({current, handleChangeSelect}) => {

    return (
        <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={current}
            onChange={handleChangeSelect}
        >
            <MenuItem value={'smartphones'}>Смартфоны</MenuItem>
            <MenuItem value={'laptops'}>Планшеты</MenuItem>
        </Select>
    )
}

export default DeviceSelect