import {FC} from "react";
import {Alert, CircularProgress} from "@mui/material";
import {LOADER_STATE} from "../../../../constants";

type Props = {
    children: JSX.Element,
    loaderState: LOADER_STATE
}

const Loader: FC<Props> = ({children, loaderState}) => {

    const renderLoader = () => {
        switch (loaderState) {
            case LOADER_STATE.PENDING || LOADER_STATE.IDLE:
                return <div className='loaderWrapper'><CircularProgress size={100}/></div>
            case LOADER_STATE.SUCCESS:
                return children
            case LOADER_STATE.FAILED:
                return <Alert severity="error">Возникла ошибка получения данных!</Alert>
            default:
                return null
        }
    }

    return (
        renderLoader()
    )
}

export default Loader