import React from 'react';
import './App.css';
import GadgetScreen from "./features/GadgetsScreen";

function App() {
  return (
     <GadgetScreen/>
  );
}

export default App;
