import axios from 'axios';

const api = axios.create({
    baseURL: 'https://dummyjson.com/',
});

api.interceptors.request.use(
    (request) => {

        return request;
    },
    (error) => {
        return Promise.reject(error);
    }
);

api.interceptors.response.use(
    (response) => {

        return response;
    },
    (error) => {
        return Promise.reject(error);
    }
);

export default api;
